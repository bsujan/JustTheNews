var post_id = 0;
const id_base = "voting_post_id_";

var current_domain = window.location.hostname;
var current_protocol = window.location.protocol;

var build_base_domain = current_protocol + "//" + current_domain;

//Give each post an ID and assign mod point buttons to it.
$('.item').each(function(){
	var story_title = $(this).find('.story_title').find('.story_link').text();
	var story_url = $(this).find('.story_title').find('a').attr('href');
	var story_date = $(this).find('.story_time_posted').data('time');
	
	//Build the modpoint buttons on the post at the current iterations.
	var buttons = build_point_buttons(story_title,story_url,post_id,story_date);
	
	$(this).find('.modpoint_manager').append(buttons);
	
	var post_vote_id_int = id_base + post_id;
	$(this).find('.modpoint_manager').attr('id',post_vote_id_int);
	
	post_id = post_id + 1;
});

//Build the boting buttons for the various modpoint types.
function build_point_buttons(post_title,post_url,post_number,story_date){
	
	post_title = post_title.trim();
	post_url = post_url.trim();
	
	var interesting_point = '<span class="facade-pointer interesting" onClick="send_modpoint(this)" data-story-url="' + post_url + '" data-story-title="' + post_title + '" data-point-type="1" data-post-id="' + post_number + '" data-post-time="' + story_date + '">Interesting</span>';
	var funny_point = '<span class="facade-pointer funny" onClick="send_modpoint(this)" data-story-url="' + post_url + '" data-story-title="' + post_title + '" data-point-type="2" data-post-id="' + post_number + '" data-post-time="' + story_date + '">Funny</span>';
	var relevant_point = '<span class="facade-pointer relevant" onClick="send_modpoint(this)" data-story-url="' + post_url + '" data-story-title="' + post_title + '" data-point-type="3" data-post-id="' + post_number + '" data-post-time="' + story_date + '">Relevant</span>';
	
	var build_points_buttons = interesting_point + " " + relevant_point + " " + funny_point;
	
	return build_points_buttons;
}

//Send the modpoint off to the backend to be processed.
function send_modpoint(identifier){	
	var current_story_title = $(identifier).data('story-title');
	var current_story_url = $(identifier).data('story-url');
	var story_point = $(identifier).data('point-type');
	var story_time = $(identifier).data('post-time');
	var session_token = $('meta[name=csrf-token]').attr('content');
	
	//If a story does not have a time assigned (as some feeds don't do) let's assign it a time.
	if(story_time.length === 0){
		var now = new Date();
		var dd = ("0" + now.getDate()).slice(-2)
		var mm = ("0" + (now.getMonth() + 1)).slice(-2)
		var yyyy = now.getFullYear();
		
		var h = now.getHours();
		var m = now.getMinutes();
		var s = now.getSeconds();
		
		var formed_time = yyyy + "-" + mm + "-" + dd + " " + h + ":" + m + ":" + s;

		story_time = formed_time;
	}
	
	var post_voting_section_id = "#" + id_base + $(identifier).data('post-id');
	
	var resource_url = build_base_domain + "/add_post_modpoint";
	
	//Send the request to the backend.
	$.ajax({
		method: 'POST', // Type of response and matches what we said in the route
		url: resource_url, // This is the url we gave in the route
		data: {"_token": session_token, 
		'story_title' : current_story_title,
		'story_url' : current_story_url,
		'story_date_time' : story_time,
		'point_type' : story_point}, // a JSON object to send back
		success: function(response){ // What to do if we succeed

			//console.log(response);
		
			//Handle the response and give the user some feedback on the page.
			
			
			var response_info = JSON.parse(response);
			
			if(response_info.voted === true){
				$(post_voting_section_id).after('<div class="text-success pts"><b>YOU VOTED!</b> - ' + response_info.reason + '</div>');
			}else{
				$(post_voting_section_id).after('<div class="text-warning pts"><b>VOTING FAILED!</b> - ' + response_info.reason + '</div>');
			}
			
			if(response_info.remove_buttons === 1){
				$(post_voting_section_id).remove();
			}
			
		},
		error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
			console.log(JSON.stringify(jqXHR));
			console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
		}
	});
}