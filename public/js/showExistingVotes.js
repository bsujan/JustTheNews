function parseExistingPoints(){
	var story_request_url = build_base_domain + "/api/stories";
	
	$.ajax({
		method: 'GET', // Type of response and matches what we said in the route
		url: story_request_url, // This is the url we gave in the route
		success: function(response){ // What to do if we succeed
			var current_stories = JSON.parse(response);
			
			for(i=0;i < current_stories.length;i++){				
				var needle_url = "a[href='" + current_stories[i].url + "']";
				
				var story_votes_id = "existing_votes_id_" + current_stories[i].id;
				
				if(jQuery(needle_url).length){
					
					var interesting_votes = '<span class="glyphicon glyphicon-eye-open"></span> ' + current_stories[i].interesting_votes + ' Interesting, ';
					var relevant_votes = '<span class="glyphicon glyphicon-time"></span> ' + current_stories[i].relevant_votes + ' Relevant, ';
					var funny_votes = '&#9786; ' + current_stories[i].funny_votes + ' Funny';
					var full_vote_string = ' <span class="existing_story_votes" style="opacity: 0;" title="This story has been voted on by at least one other reader as relevant, interesting, or funny."><small>[' + interesting_votes + relevant_votes + funny_votes + ']</small></span>';
					
					jQuery(needle_url).next('.source').after(full_vote_string);
				}
			}
			
			$('.existing_story_votes').fadeTo(1543,1);
		},
		error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
			console.log(JSON.stringify(jqXHR));
			console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
		}
	});
}

parseExistingPoints();