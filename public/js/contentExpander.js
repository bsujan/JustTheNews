$(window).on('load',function(){
	$paragraphs = $('.story_content');

	$paragraphs.each(function() {
		var totalCharacters = $(this).text().length;
		  
		if(totalCharacters > 2750){
			$(this).addClass('long_text_hider');
		}
	});
	
	$('.long_text_hider').on('click', function() {
		$(this).removeClass('long_text_hider');
	});
});