function linkCategory(){
	var source_id = $('#source_id_number').val();
	var category_id = $('#category_list').val();
	var category_name = $('#category_list option:selected').text();
		
	var process_category_link = new XMLHttpRequest();
	var page_protocol = window.location.protocol;
	var domain_hostname = window.location.hostname;
	var url = page_protocol + "//" + domain_hostname + "/";
	
	url = url + 'add-category-link/' + source_id + '/' + category_id;
	
	process_category_link.open('GET', url);
	process_category_link.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	process_category_link.onload = function() {
		var response_data = JSON.parse(process_category_link.responseText);
		
		if(response_data.success === true){
				$('#addLinkResponseOutput').addClass('pvs phs bg-success text-success');
				$('#addLinkResponseOutput').text(response_data.reason);
				var edit_category_link = ' <a href="/edit-category/' + category_id + '">[EDIT]</a>'
				$('#existingCategoryConnections').append("<li>" + category_name +"<span class=\"unsubscribe_btn\" onclick=\"unlinkCategory(this)\" data-cat-id=\"" + category_id + "\">-Remove</span>" + edit_category_link + "</li>");
			}else{
				$('#addLinkResponseOutput').addClass('pvs phs bg-warning text-warning');
				$('#removalDataOutput').text(response_data.reason);
			}
	};
	process_category_link.send();
}

function unlinkCategory(identifier){
		var category_id = $(identifier).data('cat-id');
		var source_id = $('#source_id_number').val();
		
		var process_category_unlink = new XMLHttpRequest();
		var page_protocol = window.location.protocol;
		var domain_hostname = window.location.hostname;
		var url = page_protocol + "//" + domain_hostname + "/";
		
		url = url + 'remove-category-link/' + source_id + '/' + category_id;
		
		process_category_unlink.open('GET', url);
		process_category_unlink.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		process_category_unlink.onload = function() {
			var response_data = JSON.parse(process_category_unlink.responseText);
			
			if(response_data.success === true){
				$(identifier).parent().remove();
			}else{
				$('#removalDataOutput').addClass('pvs phs');
				$('#removalDataOutput').text(response_data.reason);
			}
		};
		process_category_unlink.send();
}