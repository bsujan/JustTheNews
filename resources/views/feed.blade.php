<?php
//Loads helperFunctions controller so the sourceURLStripper function can be called to show source URLs.
use App\Http\Controllers\helperFunctions;
?>

@extends('layouts.app')

@section('title', 'All Stories')

<?php
	//Php snippet is reported back to the app.blade template. Settings these variables sets the page title and meta description.
	$page_title = "Top Stories";
	$page_meta_description = "The " . env("ALL_POST", 25) . " most recent stories from around the world. Spanning all interest and topics with reporting from reputable sources and a variety of viewpoints.";
?>

@section('content')

<div class="row">
	<div class="col-xs-8 col-md-10">
		<h1 class="page_title">All News Stories</h1><h3 class="page_subtitle">&nbsp;- The {{env("ALL_POST", 25)}} Most Recent Stories:</h3>
		<p>Welcome to <em>/all</em>. A collection of the {{env("ALL_POST", 25)}} latest stories from across <a href="/privacy-and-tos#sources">of our tracked news services.</a></p>
	</div>
	
	<div class="col-xs-4 col-md-2">
		<p><small class="source"><i>Times in US East</i></small></p>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-8 col-lg-9">
		<?php $index_count = 1; ?>
		@foreach ($items as $item)
			@if(helperFunctions::hourAhead($item->get_date('Y-m-d G:i:s')) === false)
			<div class="item">
				<h3 class="story_title">
					<a href="{{ $item->get_permalink() }}" class="story_link" target="_blank" onClick="ga('send','event','News Story Viewed','Category Feed','All stories listing');">{!! $item->get_title() !!}</a> - <span class="source">(<a href="//{{helperFunctions::sourceURLStripper($item->get_permalink())}}" title="Go to {{helperFunctions::sourceURLStripper($item->get_permalink())}}" target="_blank">{{helperFunctions::sourceURLStripper($item->get_permalink())}}</a>)</span>
				</h3>
				<div class="story_content">
					{!! $item->get_content() !!}
					<span class="read_more">LONG READ - CLICK FOR MORE &#x25BC;</span>
				</div>
				<p class="ptl pbs mbn"><small>Posted on <span class="story_time_posted" data-time="{{$item->get_date('Y-m-d H:i:s')}}">{{$item->get_date('F j, Y - g:i a')}}</span></small> | <span class="modpoint_manager"></span><span class="modpoint_message"></span></p>
			</div>
			<div class="clear"></div>
			@endif
			
			@if($index_count === 10 || $index_count === 33 || $index_count === 70)
				@if(!Auth::check())
					<div class="item_nonstory">
						@include('interstitials.feed_item_upsell')
					</div>
				@endif
			@endif
			
		<?php $index_count = $index_count + 1; ?>
		@endforeach
	</div>
  
	@if(!Auth::check())
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
			@include('interstitials.sidebar')
		</div>
	@endif  
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="/js/modpoints_listings_page.js"></script>
<script src="/js/showExistingVotes.js"></script>
<script src="/js/contentExpander.js"></script>
@endsection