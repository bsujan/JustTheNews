@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Your Dashboard</div>
				<div class="panel-body" style="margin-top: 20px;">
                    You are logged in as: {{ Auth::User()->name }} using {{Auth::User()->provider}}.
                </div>
				
				<div class="panel-body">
				<h3>User Actions:</h3>
				<ul>
					<li><a href="/">Home</a></li>
					<li><a href="/news">Your Stories</a></li>
					<li><a href="/news/all">All Stories</a></li>
					<li><a href="/subscriptions">Manage Subscriptions</a></li>
					<li><a href="/privacy-and-tos">Privacy Policy, Terms Of Service, and Current List Of News Sources</a></li>
				</ul>
				</div>
				
				<div class="panel-body">
                    <h5><a href="/news/all">View The Latest News Stories From All Sources</a></h5>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
