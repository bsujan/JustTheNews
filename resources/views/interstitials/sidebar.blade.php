<h3>Want The News Your Way?</h3>
<h3 class="deep_blue_block site_title white text-center">Signin To Customize Your Own Personal News Feed!</h3>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<p>The <b>All News Stories</b> feed is a great way to get a birds eye view of what's going on in the world right now.</p>
	<p>However sometimes you might want to just read stories on topics, and from sources that interest you the most.</p>
	<p>Signing up for an account on {{env('APP_NAME', 'Orderly.News')}} will allow you to create a custom news feed concisting only of stories from your
	favorite news sources. Signup now. It takes only a few clicks.</p>
</div>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<a href="{{ url('/auth/google') }}" class="text-center" onClick="ga('send','event','Intersitials','Account Action','Sidebar Register - Google">
		<img class="img-responsive" src="/img/google-sign-in.png" />
	</a>
</div>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<a href="{{ url('/auth/facebook') }}" class="text-center" onClick="ga('send','event','Intersitials','Account Action','Sidebar Register - Facebook">
		<img class="img-responsive" src="/img/login_with_facebook.png" />
	</a>
</div>


<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<p class="ptm">Still have questions? <a href="/about" onClick="ga('send','event','Intersitials','Link Action','Sidebar Read FAQ">Read our FAQ.</a></p>
</div>



<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<hr>
</div>