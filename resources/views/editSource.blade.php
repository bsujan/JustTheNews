@extends('layouts.app')

@section('content')

<?php
	//Php snippet is reported back to the app.blade template. Settings these variables sets the page title and meta description.
	$page_title = "Edit Source: " . $source_core_information->source_name;
	$page_meta_description = "Editing information for " . $source_core_information->source_name;
?>

<div class="row">
	<div class="col-xs-12 col-md-10">
		<div class="panel-body">
			<!-- Display Validation Errors -->
			@include('common.errors')

			<!-- Edit Source Form -->
			<form action="/updated_existing_source" method="POST" class="form-horizontal">
				{{ csrf_field() }}

				<!-- Edit Source Info Fields -->
				<input type="hidden" name="news_source_id" id="source_id_number" value="{{$source_core_information->source_id}}">
				
				<div class="row">
					<div class="col-xs-12 col-md-3">
						<p class="control-label text-right"><strong>Source Name</strong></p>
					</div>
					
					<div class="col-xs-12 col-md-9">
						<input type="text" name="news_source_name" id="source_name" class="formWidth" value="{{$source_core_information->source_name}}">
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-12 col-md-3">
						<p class="control-label text-right"><strong>Source Home Page URL</strong></p>
					</div>
					
					<div class="col-xs-12 col-md-9">
						<input type="text" name="source_home_url" id="source_home_url" class="formWidth" value="{{$source_core_information->source_home_url}}">
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-12 col-md-3">
						<p class="control-label text-right"><strong>RSS Feed Location</strong></p>
					</div>
					
					<div class="col-xs-12 col-md-9">
						<input type="text" name="rss_feed_url" id="rss_feed_location" class="formWidth" value="{{$source_core_information->rss_feed_url}}">
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-12 col-md-3 text-right">
						<label class="control-label">Notes:</label>
					</div>
					<textarea rows="2" cols="50" name="source_notes" id="source_notes" class="col-xs-12 col-md-9" maxlength="500">{{$source_core_information->notes}}</textarea>
					
				</div>
				
				<div class="row">
					<div class="col-xs-12 col-md-3">
						<p class="control-label text-right"><strong>Exclude from all feed:</strong></p>
					</div>
					
					<div class="col-xs-12 col-md-9">
						<input type="radio" name="exclude_from_all_feed" value="1" @if($source_core_information->exclude_from_feed == 1)checked @endif> Yes
						<input type="radio" name="exclude_from_all_feed" value="0" @if($source_core_information->exclude_from_feed != 1)checked @endif> No
					</div>
				</div>

				<!-- Update Source Button -->
				<div class="form-group">
					<div class="col-sm-offset-3 col-sm-6">
						<button type="submit" class="btn btn-default">
							<i class="fa fa-plus"></i> Update News Source
						</button>
					</div>
				</div>
			</form>
			<!-- End Edit Source Form -->
		</div>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-md-6">
		<h3>Existing Category Connections:</h3>
		<ul id="existingCategoryConnections">
		<?php foreach($category_connections as $connected):?>
		<li>{{$connected->category_name}} <span class="unsubscribe_btn" onclick="unlinkCategory(this)" data-cat-id="{{$connected->category_id}}">-Remove</span> <a href="/edit-category/{{$connected->category_id}}">[EDIT]</a></li>
		<?php endforeach; ?>
		</ul>
		
		<p id="removalDataOutput" class="text-warning bg-danger"></p>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-md-6 pvm">
		<h3>Assign News Service To Category:</h3>
		<div class="pvm">
			<select id="category_list" class="col-xs-12 col-md-4">
				<?php foreach($category_listings as $category_data): ?>
					<option value="{{$category_data->category_id}}">{{$category_data->category_name}}</option>
				<?php endforeach; ?>
			</select>
		</div>
		<p class="subscribe_btn" onclick="linkCategory()">+Link Category</p>
		
		<h4 id="addLinkResponseOutput"></h4>
		
		<h5><a href="/new-category">Create new category.</a></h5>
	</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="/js/linkCategory.js"></script>
@endsection