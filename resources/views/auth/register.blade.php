@extends('layouts.app')

<?php
	//PHP snippet is reported back to the app.blade template. Settings these variables sets the page title and meta description.
	$page_title = "Register";
	$page_meta_description = "Registering for {{env('APP_NAME', 'Orderly.News')}} will allow you to create a customized subscription list of news services you would like to see updates and information from.";
?>

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2 ptm">
            <div class="panel panel-default">
                <div class="panel-heading"><strong><span class="glyphicon glyphicon-hand-right"></span> Register for {{env('APP_NAME', 'Orderly.News')}}</strong></div>
                <div class="panel-body">
				<p>To view some parts of <em>{{env('APP_NAME', 'Orderly.News')}}</em> we'll need you to login with Facebook or Google.</p>
				<p>Registering for an account means you are agreeing to our <a href="/privacy-and-tos">Privacy Policy and Terms of Service.</a></p>
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
						
						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<a href="{{ url('/auth/google') }}" class="btn btn-google">
									<img style="max-width: 250px;" src="/img/google-sign-in.png" />
								</a>
							</div>
							<div class="col-md-6 col-md-offset-4">
								<a href="{{ url('/auth/facebook') }}" class="btn btn-facebook">
									<img style="max-width: 250px;" src="/img/login_with_facebook.png" />
								</a>
							</div>
						</div>
                    </form>
					
					<p>By Logging in you agree to the <a href="/privacy-and-tos#privacy">Privacy Policy</a> and <a href="/privacy-and-tos#terms">Terms of Service</a></p>
                </div>
            </div>
        </div>
    </div>
	
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
			<h1>Benefits of Registering for {{env('APP_NAME', 'Orderly.News')}}</h1>
			<h2>&bull; Customize your own personal news feed.</h2>
			<p>{{env('APP_NAME', 'Orderly.News')}} gathers news stories from a number of different sources. This can be hugely beneficial in gathering information on a current events happening
			in the world today. However when browsing something like the <a href="/news/all">Top Stories</a> page things might start to feel a bit cluttered with stories on topics you don't care about.</p>
			<p>Signing up for an account on {{env('APP_NAME', 'Orderly.News')}} will allow you to customize a news feed personalized with stories from sources that interest you.</p>
			<p>Once you've registered visit the <a href="/news">Your Stories</a> tab to get started on customizing your news feed.</p>
			<h2>&bull; Give mod points to stories to allow people to know they are interesting, relevant, or funny.</h2>
			<p>You may have noticed these little icons and buttons hanging around by each news story or article. These are mod points.</p>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<img src="/img/PopularStoriesModPoints.png" class="img-responsive" />
			<p>Mod Points on the <a href="/popular-articles-and-stories">Popular Stories</a> page.</p>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<img src="/img/AllStoriesModPoints.png" class="img-responsive" />
				<p>Mod Points on the <a href="/news/all">All Stories</a> page.</p>
			</div>
			<p>Mod Points are the {{env('APP_NAME', 'Orderly.News')}} version of a karma system. They don't change the order of post but they do signal to other readers
			what stories are good and are worth their time. Additionally all stories that are given Mod Points are forever kept in the <a href="/popular-articles-and-stories">Popular Stories</a>
			tab.</p>
			<h2>&bull; Still have more questions?</h2>
			<p>We understand. Check out our <a href="/about">FAQ for more information about {{env('APP_NAME', 'Orderly.News')}}.</a></p>
		</div>
	</div>
</div>
@endsection
