@extends('layouts.app')

<?php
	//PHP snippet is reported back to the app.blade template. Settings these variables sets the page title and meta description.
	$page_title = "Login";
	$page_meta_description = "Login to {{env('APP_NAME', 'Orderly.News')}} using Google or Facebook to see your customized feed of the latest news stories from subscribed reporters list. If you don't have an account don't worry. Create one quickly and easily.";
?>

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2 ptm">
            <div class="panel panel-default">
                <div class="panel-heading"><strong><span class="glyphicon glyphicon-user"></span> Login to {{env('APP_NAME', 'Orderly.News')}}</strong></div>
				<p>To view some parts of <em>{{env('APP_NAME', 'Orderly.News')}}</em> we'll need you to login with Facebook or Google.</p>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<a href="{{ url('/auth/google') }}" class="btn btn-google">
									<img style="max-width: 250px;" src="/img/google-sign-in.png" />
								</a>
							</div>
							<div class="col-md-6 col-md-offset-4">
								<a href="{{ url('/auth/facebook') }}" class="btn btn-facebook">
									<img style="max-width: 250px;" src="/img/login_with_facebook.png" />
								</a>
							</div>
						</div>
                    </form>
					
					<p>By Logging in you agree to the <a href="/privacy-and-tos#privacy">Privacy Policy</a> and <a href="/privacy-and-tos#terms">Terms of Service</a></p>
                </div>
            </div>
        </div>
    </div>
	
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
			<h1>Please log back into {{env('APP_NAME', 'Orderly.News')}}</h1>
			<h2>&bull; In order to access your personalized news feed.</h2>
			<p>Your personalized news feed is stored with your account information used to log into {{env('APP_NAME', 'Orderly.News')}}. Please sign back into access it.</p>
			<h2>&bull; Add Mod Points to Stories and Articles?</h2>
			<p>Mod Points can only be given to stories and articles by logged in users.</p>
			<h2>&bull; Not Registered or Have Questions?</h2>
			<p>We understand. Check out our <a href="/about">FAQ for more information about {{env('APP_NAME', 'Orderly.News')}}.</a></p>
			<p>Or simply head on over to the <a href="/register">Register page</a> to register for an account.</p>
		</div>
	</div>
</div>
@endsection
