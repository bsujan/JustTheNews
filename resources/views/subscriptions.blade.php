@extends('layouts.app')

@section('title', 'Manage Subscriptions')

@section('content')

<?php
	//PHP snippet is reported back to the app.blade template. Settings these variables sets the page title and meta description.
	$page_title = "Manage Your Subscriptions";
	$page_meta_description = "View all of the active news sources on " . env('APP_NAME', 'Orderly.News') . " and subscribe to the ones which are most interesting to you. Stories from these sources will be displayed on the Your Stories tab.";
?>

<div class="row">
	<!-- Display Validation Errors -->
	@include('common.errors')

	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<h1>Manage Your Subscriptions:</h1>
	@if(Auth::User()->role == 'admin')
		<p>Add: <a href="/new-source">New Source</a> | <a href="/new-category">New Category</a></p>
	@endif
	</div>
	
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<label for="search-highlight" class="search_sources">Search Sources: </label>
			<input id="search-highlight" class="search_sources_box" name="search-highlight" placeholder="Narrow Shown Sources" type="text" data-list=".sources_list" autocomplete="off">
		
		<ul class="list-unstyled sources_list">
		@foreach ($subscribed_list as $single_subscription)
		<li>
			<h2><a href="{{$single_subscription->source_home_url}}" target="_blank">{{$single_subscription->source_name}}</a></h2>
			@if($single_subscription->notes != null)
				<p><span class="text-warning">WARNING:</span> {{$single_subscription->notes}} - As a result this feed is excluded from /news/all.</p>
			@endif
			@if($single_subscription->current_sub == true)
				<p onclick="manageSubscription(this)" data-value="{{$single_subscription->source_id}}" data-event="unsubscribe" class="unsubscribe_btn hideseek_ignore_sub_buttons">-unsubscribe</p>
			@else
				<p onclick="manageSubscription(this)" data-value="{{$single_subscription->source_id}}" data-event="subscribe" class="subscribe_btn hideseek_ignore_sub_buttons">+subscribe</p>
			@endif
			
			@if(Auth::User()->role == 'admin')
				<p><a href="/edit-source/{{$single_subscription->source_id}}">Edit <i>{{$single_subscription->source_name}}</i></a></p>
			@endif
		</li>
		@endforeach
		</ul>
	</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="/js/subscribe.js"></script>
<script src="/js/jquery.hideseek.min.js"></script>
<script>
	$('#search-highlight').hideseek({
	  highlight: false,
	  ignore: '.hideseek_ignore_sub_buttons'
	});
</script>
   
@endsection