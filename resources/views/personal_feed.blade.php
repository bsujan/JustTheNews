<?php
//Loads helperFunctions controller so the sourceURLStripper function can be called to show source URLs.
use App\Http\Controllers\helperFunctions;
?>

@extends('layouts.app')

<?php
	//Php snippet is reported back to the app.blade template. Settings these variables sets the page title and meta description.
	$page_title = "Your Stories";
	$page_meta_description = "A collection of stories from your subscribed feeds.";
?>

@section('content')

<div class="row">
	<div class="col-xs-8 col-md-10">
		<h1 class="page_title">Your Stories</h1><h3 class="page_subtitle">&nbsp;- The {{env("USER_POST", 25)}} Most Recent Stories from Your Subscriptions Pool:</h3>
	</div>

	<div class="col-xs-4 col-md-2">
		<p class="white_subtext_header"><small class="source"><i>All Times in US East</i></small></p>
	</div>
</div>
  
<div class="row" id="stories">
	@if(count($user_stories['items']) > 0)
	<div class="col-xs-12 col-sm-12 col-md-8 col-lg-9">
		@foreach ($user_stories['items'] as $item)
			@if(helperFunctions::hourAhead($item->get_date('Y-m-d G:i:s')) === false)
			<div class="item">
				<h3 class="story_title">
					<a href="{{ $item->get_permalink() }}" class="story_link" target="_blank" onClick="ga('send','event','News Story Viewed','Category Feed','Personalized News Feed');">{!! $item->get_title() !!}</a> - <span class="source">(<a href="//{{helperFunctions::sourceURLStripper($item->get_permalink())}}" title="Go to {{helperFunctions::sourceURLStripper($item->get_permalink())}}" target="_blank">{{helperFunctions::sourceURLStripper($item->get_permalink())}}</a>)</span>
				</h3>
				<div class="story_content">
					{!! $item->get_content() !!}
					<span class="read_more">LONG READ - CLICK FOR MORE &#x25BC;</span>
				</div>
				<p class="ptl pbs mbn"><small><span class="story_time_posted" data-time="{{$item->get_date('Y-m-d H:i:s')}}">{{$item->get_date('F j, Y - g:i a T')}}</span></small> | <span class="modpoint_manager"></span><span class="modpoint_message"></span></p>
			</div>
		
			<div class="clear"></div>
			@endif
	  @endforeach
  </div>
  
  <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
	<h3>Your Subscriptions:</h3>
	<ul>
		@foreach($user_subscriptions as $source)
			<li><a href="{{$source->source_home_url}}" target="_blank">{{$source->source_name}}</a></li>
		@endforeach
	</ul>
  </div>
  @else
	  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<h2>IT LOOKS LIKE YOU HAVEN'T SUBSCRIBED TO ANY SOURCES YET!</h2>
		<p>If you want to see stories from your favorite news sources head on over to the <a href="/subscriptions">Manage Subscriptions</a> page and press 
		<span class="bg-success text-success pvs phs"><strong>+subscribe</strong></span> on the news sources that interest you.</p>
	  </div>
	  
	  <div class="clear pvl"></div>
	  
	  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
		<img src="https://i.imgur.com/wLVkmsg.jpg" class="img-responsive" />
	  </div>
	  
	  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
		<img src="https://i.imgur.com/Q2z4C9S.jpg" class="img-responsive" />
	  </div>
	  
	  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
		<img src="https://i.imgur.com/U9RnouY.jpg" class="img-responsive" />
	  </div>
	  
	  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
		<img src="https://i.imgur.com/8ym5nwm.jpg" class="img-responsive" />
	  </div>
	  
	  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mtm">
		  <p>Just some pretty photos because we can.</p>
		  <p>Seriously though go <a href="/subscriptions">Manage Your Subscriptions</a>.</p>
	  </div>
  @endif
</div>
  
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="/js/modpoints_listings_page.js"></script>
<script src="/js/showExistingVotes.js"></script>
<script src="/js/contentExpander.js"></script>
@endsection