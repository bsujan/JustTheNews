@extends('layouts.app')

@section('title', 'Top Stories')

@section('content')
<?php
//This will convert all the times of post to US East Coast time.
//Again later we should probably wrap this out to be handled better or localize it for the user.
date_default_timezone_set('America/New_York'); 
?>

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<h3 class="page_title">The Latest Stories By Category<sub class="text-danger">[beta]</sub></h3>
	</div>
</div>

<div class="row">
	<?php $row_split = 1; //Row split counts and is checked for the remainder every 3 items.?>
	@foreach($frontpage_data as $single_category)
		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
			<h2><a href="/cat/{{$single_category->category_url}}" title="View a larger selection of stories from the {{$single_category->category_name}} category." onClick="ga('send','event','Home Page','Main Category Clicked','{{$single_category->category_name}}');">{{$single_category->category_name}}</a>:</h2>
			@foreach($single_category->front_page_stories['items'] as $story)
				<div class="homepage-story-block pvs">
					<p class="mbn">
						<a href="{{$story->get_permalink()}}" class="homepage-story-link" onClick="ga('send','event','News Story Viewed','Homepage Featured Category','{{$single_category->category_name}}');" target="_blank">
							{!!$story->get_title()!!}
						</a>
					</p>
					<p class="text-muted reveal-me" style="opacity: 0;"><small>{{$story->get_date('g:i a')}} - <span class="homepage-story-source"></span></small></p>
				</div>
			@endforeach
				<p><a href="/cat/{{$single_category->category_url}}" title="View more recent news updates and articles in the {{$single_category->category_name}} category beyond the 10 most recent." onClick="ga('send','event','Home Page','Main Category View All Clicked','{{$single_category->category_name}}');"><em>View all stories in {{$single_category->category_name}}&raquo;</em></a></p>
			@if(Auth::user() && Auth::User()->role == 'admin')
				<p><a href="/edit-category/{{$single_category->category_id}}" class="text-warning">Edit <i>{{$single_category->category_name}}</i></a></p>
			@endif
		</div>
		
		@if($row_split % 3 == 0)
			<div class="col-xs-12 col-sm-12 col-ms-12 col-lg-12 clear"></div>
		@endif
		<?php $row_split = $row_split + 1; ?>
	@endforeach
</div>

@if(!Auth::check())
	@include('interstitials.homepage_upsell')
@endif

@if(count($frontpage_features) > 0)
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<h2 class="h1 ptl mbs">Featured Categories:</h2>
	</div>
	<?php $feature_split = 1; //Row split counts and is checked for the remainder every 2 items.?>
	@foreach($frontpage_features as $feature)
	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
		<h2 class="mvs"><a href="/cat/{{$feature->category_url}}" title="See the latest news stories and articles relating to {{$feature->category_name}}." onClick="ga('send','event','Home Page','Featured Category Clicked','{{$single_category->category_name}}');">{{$feature->category_name}}</a></h2>
		@if($feature->category_description != null)
			<p>{{$feature->category_description}}</p>
		@endif
	</div>
	
	@if($feature_split % 2 == 0)
		<div class="col-xs-12 col-sm-12 col-ms-12 col-lg-12 clear"></div>
	@endif
	<?php $feature_split = $feature_split + 1; ?>
	@endforeach
</div>
@endif

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<p><small><span class="text-danger">[Beta]</span> Due to the way some feeds are formed some stories in any given category might not be entirely contextural. We are working to improve this over time.</small></p>
	</div>
</div>

{{--Upcoming future work will be to consolidate all of the javascript to one page, and only a few files so that I don't have to do this all the time.--}}
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="/js/homeLinksInfo.js"></script>

@endsection