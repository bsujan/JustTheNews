<!DOCTYPE html>
<html lang="en">
    <head>
		<?php
			//Logic to work out what the page title and meta description should be at any given time. 
			if(isset($page_title)){
				$the_page_title = $page_title . " - " . env('APP_NAME', 'Orderly.News');
			}else{
				$the_page_title = env('APP_NAME', 'Orderly.News') . " - Information as it happens.";
			}
			
			if(isset($page_meta_description)){
				$the_page_meta_description = $page_meta_description;
			}else{
				$the_page_meta_description = env('APP_NAME', 'Orderly.News') . " is a news aggregator designed to retrieve news stories from a wide variety and sources and viewpoints. Stories are relayed in the order they are posted without
				algorithms or editing. Allowing you to get an unfiltered view of what is happening in the world today.";
			}

		?>
		<title><?= $the_page_title ?></title>
		<meta name="description" content="<?= $the_page_meta_description ?>">
		
		<meta property="og:title" content="<?= $the_page_title ?>" />
		<meta property="og:description" content="<?= $the_page_meta_description ?> - View the latest news at {{env('APP_NAME', 'Orderly.News')}}." />
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <!-- Custom CSS -->
		<link rel="stylesheet" href="/css/style.css" type="text/css" >
		<link rel="stylesheet" href="/css/ConsistentSpacers.css" type="text/css" >
		
		<meta name="csrf-token" content="{{ Session::token() }}"> 
		
		<!--Whole Bunch of Favicons-->
		<link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
		<link rel="manifest" href="/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">
		<!--End a Whole Bunch of Favicons-->
    </head>

    <body>
    	<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-8">
					<h1 class="site_title"><a href="/">{{env('APP_NAME', 'Orderly.News')}}</a></h1><h2 class="site_subtitle">&nbsp;- Information as it happens.</h2>
				</div>
				
				<div class="col-xs-12 col-md-4">
						@if(Auth::check())
							<h5>Logged in: {{ Auth::User()->name }} | <a href="/logout" onClick="ga('send','event','Header','Account Action','Logout">Logout</a></h5>
						@else
							<h5>
								<a href="/login" onClick="ga('send','event','Header','Account Action','Login">Login</a> | <a href="/register" onClick="ga('send','event','Header','Account Action','Register">Register</a>
							</h5>
						@endif
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12 col-md-12">
				<nav id="navigation">
					<ul class="nav_link_list">
						<li><a href="/" onClick="ga('send','event','Navigation','Top Navbar','Home');">Home</a></li>
						<li><a href="/news/all" onClick="ga('send','event','Navigation','Top Navbar','All Stories');">All Stories</a></li>
						<li><a href="/news" onClick="ga('send','event','Navigation','Top Navbar','Personal News Feed');">Your Stories</a></li>
						<li><a href="/popular-articles-and-stories" onClick="ga('send','event','Navigation','Top Navbar','Popular Stories');">Popular Stories</li>
						<li><a href="/categories" onClick="ga('send','event','Navigation','Top Navbar','Categories');">Categories</a></li>
						<li><a href="/subscriptions" onClick="ga('send','event','Navigation','Top Navbar','Subscription Manager');">Manage Subscriptions</a></li>
					</ul>
				</nav>
				</div>
			</div>

			@yield('content')
			
			<div class="row">
				<div class="col-sm-12 col-xs-12 col-md-12 col-lg-12">
					<ul class="pln" style="list-style: none; border-top: 1px solid grey; padding-top: 8px;">
						<li style="display: inline-block;"><b>Helpful Links:</b></li>
						<li style="display: inline-block;"><a href="/" onClick="ga('send','event','Navigation','Footer Navigation','Home');">Home</a> | </li>
						<li style="display: inline-block;"><a href="/about" onClick="ga('send','event','Navigation','Footer Navigation','About Orderly.News');">About</a> | </li>
						<li style="display: inline-block;"><a href="/news" onClick="ga('send','event','Navigation','Footer Navigation','Personal News Feed');">Your Stories</a> | </li>
						<li style="display: inline-block;"><a href="/news/all" onClick="ga('send','event','Navigation','Footer Navigation','All News Stories');">All Stories</a> | </li>
						<li style="display: inline-block;"><a href="/subscriptions" onClick="ga('send','event','Navigation','Footer Navigation','Subscriptions Manager');">Manage Subscriptions</a> | </li>
						<li style="display: inline-block;"><a href="/privacy-and-tos" onClick="ga('send','event','Navigation','Footer Navigation','Privacy Policy and Terms');">Privacy Policy and Terms Of Service</a></li>
						<li style="display: inline-block;">&copy; 2018 - {{env('APP_NAME', 'Orderly.News')}}</li>
					</ul>
				</div>
			</div>
		</div>
	
	@if(env('APP_ENV', 'production') == 'production')
	<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-32585378-6', 'auto');
		  ga('send', 'pageview');
	</script>
	<script type='text/javascript'>
		window.__lo_site_id = 109530;

		(function() {
			var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
			wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
		  })();
	</script>
	@else
		<!--Not a production environment. Anayltics is not enabled.-->
	@endif
    </body>
</html>