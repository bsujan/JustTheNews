@extends('layouts.app')

@section('content')

<?php
	//PHP snippet is reported back to the app.blade template. Settings these variables sets the page title and meta description.
	$page_title = "Create a new source";
	$page_meta_description = "Add a new source.";
?>

<div class="panel-body">
	<!-- Display Validation Errors -->
	@include('common.errors')

	<!-- New Task Form -->
	<form action="/new-source-added" method="POST" class="form-horizontal">
		{{ csrf_field() }}

		<!-- Task Name -->
		<div class="form-group">
			<div class="col-xs-12 col-md-12">
				<label for="task" class="col-xs-12 col-md-3">Source Name:</label>
				<input type="text" name="news_source_name" id="source_name" class="col-xs-12 col-md-9">
			</div>

			<div class="col-xs-12 col-md-12">
			<label for="task" class="col-xs-12 col-md-3">Source Home Page URL:</label>
				<input type="text" name="source_home_url" id="source_home_url" class="col-xs-12 col-md-9">
			</div>
			
			<div class="col-xs-12 col-md-12">
			<label for="task" class="col-xs-12 col-md-3">RSS Feed Location:</label>
				<input type="text" name="rss_feed_url" id="rss_feed_location" class="col-xs-12 col-md-9">
			</div>
			
			<div class="col-xs-12 col-md-12">
			<label for="task" class="col-xs-12 col-md-3">Notes:</label>
				<textarea rows="2" cols="50" name="source_notes" id="source_notes" class="col-xs-12 col-md-9" maxlength="500"></textarea>
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12 col-md-3">
				<p class="control-label text-right"><strong>Exclude from all:</strong></p>
			</div>
			
			<div class="col-xs-12 col-md-9">
				<input type="radio" name="exclude_from_all_feed" value="1"> Yes
				<input type="radio" name="exclude_from_all_feed" value="0" checked> No
			</div>
		</div>

		<!-- Add Task Button -->
		<div class="form-group">
			<div class="col-sm-offset-3 col-sm-6">
				<button type="submit" class="btn btn-default">
					<i class="fa fa-plus"></i> Add New Source
				</button>
			</div>
		</div>
	</form>
</div>
@endsection