@extends('layouts.app')

<?php
	//Php snippet is reported back to the app.blade template. Settings these variables sets the page title and meta description.
	$page_title = "Popular Stories";
	$page_meta_description = "A collection of news stories and articles voted on the by the community as interesting, relevant, or funny to what's going on in the world today. Just browse or signup for an account to get involved yourself.";
?>

@section('content')
<div class="row">
	<div class="col-xs-8 col-md-10">
		<h1 class="page_title">Popular Stories</h1><h3 class="page_subtitle">&nbsp;- A collection of the best stories as choosen by the community.</h3>
		<p>A collection of the most popular, interesting, relevant, and funny news stories and articles from around the world as voted on by the community.</p>
	</div>
	
	<div class="col-xs-4 col-md-2">
		<p><small class="source"><i>Times in US East</i></small></p>
	</div>
</div>

<div class="row">
	@foreach($feed_data as $news_story)
		<div class="col-xs-12 col-md-10">
		<div class="item">
			<h2 class="story_title h3">
				<a href="{{ $news_story->url }}" class="story_link" target="_blank" onClick="ga('send','event','News Story Viewed','Most Popular Stories','Most Popular Story');">{!! $news_story->title !!}</a>
				- <span class="source">(<a href="//{{$news_story->source_url}}" title="Go to {{$news_story->source_url}}" target="_blank">{{$news_story->source_url}}</a>)</span>
			</h2>
			<div class="story_mod_points">
				<span class="interesting" title="{{$news_story->interesting_votes}} voted this post as interesting."><span class="glyphicon glyphicon-eye-open"></span> {{$news_story->interesting_votes}} Interesting</span> 
				<span class="relevant" title="{{$news_story->relevant_votes}} voted this post as relevant."><span class="glyphicon glyphicon-time"></span> {{$news_story->relevant_votes}} Relevant</span> 
				<span class="funny" title="{{$news_story->funny_votes}} voted this post as funny.">&#9786; {{$news_story->funny_votes}} Funny</span></div>
			<p class="ptl pbs mbn"><small>Posted on <span class="story_time_posted" data-time="{{$news_story->date}}">{{$news_story->date}}</span></small> | <span class="modpoint_manager"></span><span class="modpoint_message"></span></p>
		</div>
	</div>

	<div class="clear"></div>
	@endforeach
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		{{--Pagination Links Generator--}}
		{{ $feed_data->links() }}
	</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="/js/modpoints_listings_page.js"></script>
@endsection