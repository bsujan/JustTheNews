@extends('layouts.app')

<?php
	//Php snippet is reported back to the app.blade template. Settings these variables sets the page title and meta description.
	$page_title = "About and FAQ";
	$page_meta_description = "Want to better understand what {{env('APP_NAME', 'Orderly.News')}} is? Read our FAQ and get information on how our site filters news and different functionalities work.";
?>

@section('content')

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<h1 id="FAQHeader">{{env('APP_NAME', 'Orderly.News')}} Frequently Asked Questions:</h1>
		
		<h2 id="whatIs">What is {{env('APP_NAME', 'Orderly.News')}}?</h2>
		<p>{{env('APP_NAME', 'Orderly.News')}} is a news aggregatorway designed to view news stories and articles from a wide variety of sources with differing viewpoints and opinions.</p>
		<p>In recent years platforms like Google, Facebook, and Twitter have moved towards algorithmic content models in which they supply you with news stories and headlines that they have tailored
		to suit your personal viewpoints.</p>
		<p>There is nothing wrong with this. It has allowed many people including some of us who work on this project to find interesting content that we might not have seen otherwise. There is a definate
		time and place for these types of content distribuation models in the online world.</p>
		<p>However we felt there are other times in which it would be nice to see news on the days events in an untailored fashion. With the most recently reported story at the top of the feed, and the oldest reported story last.
		No filtering of sources to cater to your own view point, and better ability to understand events as they happen in the order they happen.</p>
		<p>We hope in some ways this helps to combat the dreaded fake news we've all heard so much about in the last year or so. We only get stories from reputable journalist and organizations.
		These organizations might have different opinions on any given topic but you can be assured they are reputable, honest, and do not knowingly publish falsehoods.</p> 
		<p>In short {{env('APP_NAME', 'Orderly.News')}} exist in order to allow you to get news as it happens, from many different viewpoints, and then allows you to make a decision based on the knowledge you've gained.</p>
		
		<h2>How often do you update the news feed?</h2>
		<p>Each source is crawled rougly every 15 minutes.</p>
		<p>News feeds are primarily crawled using <a href="https://en.wikipedia.org/wiki/RSS">RSS feeds</a> from sources themselves. We then temporarily cache the data from that feed in order to improve
		performance and loading times so that you can get to what you're looking for faster.</p>
		<p>Feeds are crawled asynchronously so some might take <i>slightly</i> longer to be updated.</p>

		<h2>What benefits do I get by signing up for {{env('APP_NAME', 'Orderly.News')}}?</h2>
		<h3>&bull; Customize your own personal news feed.</h3>
		<p><a href="/login">Registering</a> for or <a href="/register">logging into</a> {{env('APP_NAME', 'Orderly.News')}} gives you a number of benefits.</p>
		<p>The biggest benefit of signing up for {{env('APP_NAME', 'Orderly.News')}} is that it will allow you to customize the <a href="/news">Your Stories tab</a>.</p>
		<p>The Your Stories tab allows you to tailor a personalized news feed consisting only of the latest stories, and articles from your favorite sources as they publish news content.
		The underlying idea is the same as stated above in that stories are delievered to you in the order they occur. However we want people to be able to tailor their news reading expereince somewhat.
		We know you might not be interested in sports news so why should you have to read past it in your news feed?</p>
		<p>Signing in and building a custom feed of our stories from sources that interest you will hopefully cut down on some of the clutter you see in the <a href="/news/all">All Stories</a> tab.</p>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<img src="/img/ManageSubscriptionsPage.png" class="img-responsive" />
			<p>A view of the <a href="/subscriptions">Manage Subscriptions</a> tab.</p>
		</div>
		<p>Customizing you feed is easy. Simple navigate to the <a href="/subscriptions">Manage Subscriptions</a> tab and press <span class="subscribe_btn">+subscribe</span> button on any news
		source you find interesting. Stories from all your subscribed news providers will then appear in the <a href="/news">Your Stories</a> tab.
		
		<h3>&bull; Give mod points to stories to allow people to know they are interesting, relevant, or funny.</h3>
		<p>Mod points are our form of a karma system. Many websites employ karma systems around the internet. Some notable example are <a href="https://slashdot.org/faq/karma.shtml">Slashdot</a>,
		<a href="https://stackoverflow.com/help/whats-reputation">Stack Overflow</a>, and <a href="https://www.reddit.com/wiki/faq#wiki_why_should_i_try_to_accumulate_karma.3F">reddit.</a></p>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<img src="/img/PopularStoriesModPoints.png" class="img-responsive" />
			<p>Mod Points on the <a href="/popular-articles-and-stories">Popular Stories</a> page.</p>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<img src="/img/AllStoriesModPoints.png" class="img-responsive" />
			<p>Mod Points on the <a href="/news/all">All Stories</a> page.</p>
		</div>
		<p>You may have noticed these little buttons and icons hanging around reading <span class="interesting">interesting</span>, <span class="relevant">relevant</span>, or <span class="funny">funny</span>,
		these are mod points.</p>
		<p>You can click on these buttons for any post and it will add a mod point to that news story. Everyone else browsing {{env('APP_NAME', 'Orderly.News')}} will then see mod points given to that story.</p>
		<p>Only users who are logged into {{env('APP_NAME', 'Orderly.News')}} can give a story mod points.</p>
		
		<h2>What do mod points effect?</h2>
		<p>Mod points don't really effect anything. News stories will still be listed in the feed in the order they are published.</p>
		<p>Mod points basically act as a community marker to signify to others that an article is worth their time in one way or another.</p>
		<p>Articles and stories that are given mod points are forever archived in the <a href="/popular-articles-and-stories">Popular Stories</a> tab. The popular stories tab functions like
		all other listings on the site in that articles are listed based on date and time published. However the popular stories tab does seek to be somewhat of a distillation of the most interesting stories.</p>
		<p>Mod Points are split into the following 3 categories:</p>
		<ul>
			<li>Interesting - Stories modded interesting can be anything you find interesting. An interesting sports statistic, a new scientifics discovery, or an album review for your favorite artist.</li>
			<li>Relevant - Stories with relevant mods should be things relating to topical current events in the world today.</li>
			<li>Funny - This mod point is reserved for stories that are funny, fun, or bring a smile to your face. A lot of the news in the world today is overwhelmingly negative. This is to highlight stories
			that make people feel good.</li>
		</ul>
		<p>As you can see mod points are a somewhat loose system. We have outlines for how you should use them but at the end of the day the community can award them wherever they see fit.</p>
		<p>In order to stop spamming and encourage actual reading of articles it user are limited to <em>{{env('HOURLY_VOTE_LIMIT', 8)}} mod points per hour.</em> You will recieve a cooldown for one hour once
		this limit is reached.</p>
		
		<h2>How do categories work?</h2>
		<p>Our news categories system is pretty straight forward. Feeds are placed into categories based on the typical topics they report on.</p>
		<p>This means a site that often reports on technology such as <a href="https://arstechnica.com/">Ars Technica</a> will have its stories show up in the <a href="/cat/technology">technology category.</a>
		But it won't show up somewhere like the <a href="/cat/sports">sports category</a> along side news services like <a href="http://www.espn.com/">ESPN.</a></p>
		<p>A news service can exist in multiple categories and when you're browsing a category you will see all news services within it located in the sidebar.</p>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<img src="/img/CategorySidebar.png" class="img-responsive" />
			<p>Sidebar showing news sources for the  <a href="/news/all">US Politics</a> category.</p>
		</div>
		<p>Sometimes these sources shared across categories will cause some odd or seemingly missmatched stories. We're working on improving and filtering out these stories as times
		goes along. Sometimes the simple solution is to just remove a source from a category. As a result categories and their content will often be subject to change.</p>
		
		<h2 id="sources">Where does the news on {{env('APP_NAME', 'Orderly.News')}} come from?</h2>
		<p>We currently run {{env('APP_NAME', 'Orderly.News')}} on an approved services basis. Keeping the number of feeds we crawl limited helps us find and workout various kinks and bugs in the system.</p>
		<p>Below you'll find a list of all the feeds we currently crawl and support here on <em>{{env('APP_NAME', 'Orderly.News')}}.</em> You can see the latest news from all services by visiting our <a href="/news/all"><i>/all</i> feed.</a>
		You can also choose which services you see news from by <a href="/subscriptions">managing your subscriptions</a> and then viewing them at <a href="/news"><i>/news.</i></a>
		<p>Finally, you can view news by category. To see all currently active categories visit the <a href="/categories">Categories</a> tab.</p>

		<ul>
			{{-- List off approved news sources into the page. --}}
			@if(count($news_sources) > 0)
				@foreach($news_sources as $news_source)
					<li>{{$news_source->source_name}} @if($news_source->notes != null) - <b>NOTE:</b>{{$news_source->notes}}@endif</li>
				@endforeach
			@endif
		</ul>

		<p>Have a feed you'd like to see that we currently don't support?</p>
		<p>Send us an email with a link to your suggestion at <a href="mailto:suggestions@orderly.news">suggestions[at]orderly.news</a></p>
		
		<h2 id="sources">What does NOTE mean next to a source?</h2>
		<p>Some sources incorrectly report data or report data in ways that is hard to manipulate and parse. As a result these sources are excluded from appearing on the home page
		of the site, in category feeds, and in the all atories feed. However we realize that some people might want to still recieve news from these services. They can be subscribed to
		and added to your personal feed.</p>
		<p>The note field simply exist as a way to give a breif rationale for why a source/service is excluded from various feeds.</p>
	</div>
</div>

@endsection