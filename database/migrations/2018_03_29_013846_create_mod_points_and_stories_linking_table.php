<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModPointsAndStoriesLinkingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mod_points', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('story_id');
			$table->integer('user_id');
			$table->integer('mod_type');
            $table->timestamp('voted_on');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mod_points');
    }
}
