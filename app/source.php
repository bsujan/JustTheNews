<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class source extends Model
{
	
	public function saveNewSource(Request $request){
		$validator = Validator::make($request->all(), [
			'news_source_name' => 'required|max:255|unique:sources,source_name',
			'source_home_url' => 'required|url|max:255',
			'rss_feed_url' => 'required|url|unique:sources,rss_feed_url|max:255',
			'exclude_from_all_feed' => 'required|boolean|integer',
			'source_notes' => 'max:500'
		]);

		if ($validator->fails()) {
			return back()
				->withInput()
				->withErrors($validator);
		}

		$this->source_name = $request->news_source_name;
		$this->source_home_url = $request->source_home_url;
		$this->rss_feed_url = $request->rss_feed_url;
		$this->exclude_from_feed = $request->exclude_from_all_feed;
		$this->notes = $request->source_notes;
		$this->save();
		

		return redirect('/subscriptions');
	}
	
	public function updateSource(Request $request){
		$validator = Validator::make($request->all(), [
			'news_source_id'=>'required|exists:sources,source_id',
			'news_source_name' => 'required|max:255|unique:sources,source_name,'.$request->news_source_id.',source_id',
			'source_home_url' => 'required|url|max:255',
			'rss_feed_url' => 'required|url|max:255|unique:sources,rss_feed_url,'.$request->news_source_id.',source_id',
			'exclude_from_all_feed' => 'required|boolean|integer',
			'source_notes' => 'max:500',
		]);

		if ($validator->fails()) {
			return back()
				->withInput()
				->withErrors($validator);
		}

		$this->where('source_id',$request->news_source_id)->update([
		'source_name'=>$request->news_source_name,
		'source_home_url'=>$request->source_home_url,
		'rss_feed_url'=>$request->rss_feed_url,
		'exclude_from_feed' => $request->exclude_from_all_feed,
		'notes' => $request->source_notes]);

		$redirect_link = '/edit-source/' . $request->news_source_id;
		
		return redirect($redirect_link);
	}
	
	public function fetchExistingSource($news_source_id){
		return $this->where('source_id',$news_source_id)->first();
	}
}
