<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use DB;

class RandomSourcesAndRandomCats extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //Grabs all images from header image folder. Displayers one in header of site.
        view()->composer('interstitials.homepage_upsell',function($view){

            $source_names = DB::table('sources')->pluck('source_name');
			$category_names = DB::table('categories')->pluck('category_name');
			
			$random_sources = $source_names->random(3);
			$random_categories = $category_names->random(3);
			
            $view->with(['random_sources' => $random_sources, 'random_categories' => $random_categories]);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
