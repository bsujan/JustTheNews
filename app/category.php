<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class category extends Model
{
    //Normally the model assumes the plural of the table with an 's' at the end. That won't work nicely with categories. So this overrides the table the model selects.
	protected $table = 'categories';
	protected $primaryKey = 'category_id';
	
	public function addNewCategory($create_category_request){
	
		$validate_category_request = Validator::make($create_category_request->all(),[
			'category_name' => 'required|unique:categories,category_name|max:140',
			'category_url' => 'required|unique:categories,category_url|max:150',
			'category_slug' => 'required|unique:categories,category_slug|max:40',
			'category_description' => 'max:10000',
			'category_meta_description' => 'max:500',
		]);
	

		if ($validate_category_request->fails()){
			return back()->withInput()->withErrors($validate_category_request);
		}
		
		$this->category_name = $create_category_request->category_name;
		$this->category_url = $create_category_request->category_url;
		$this->category_slug = $create_category_request->category_slug;
		$this->front_page = false;
		$this->front_page_feature = false;
		$this->category_description = $create_category_request->category_description;
		$this->category_meta_description = $create_category_request->category_meta_description;
		$this->save();
		

		return redirect('/#category_created');
	}
	
	//Validate and then update an existing categories data.
	public function processCategoryUpdate($category_update_request){
		
		$category_validation = Validator::make($category_update_request->all(), [
			'category_id_number'=>'required|exists:categories,category_id',
			'category_name' => 'required|max:140|unique:categories,category_name,'.$category_update_request->category_id_number.',category_id',
			'category_url' => 'required|alpha_dash|max:150|unique:categories,category_url,'.$category_update_request->category_id_number.',category_id',
			'category_slug' => 'required|max:40|max:150|unique:categories,category_url,'.$category_update_request->category_id_number.',category_id',
			'show_front_page' => 'required|boolean|integer',
			'front_page_featurette' => 'required|boolean|integer',
			'category_description' => 'max:10000',
			'category_meta_description' => 'max:500',
		]);

		if ($category_validation->fails()) {
			return back()->withInput()->withErrors($category_validation);
		}
		
		$this->where('category_id','=',$category_update_request->category_id_number)->update([
			'category_name' => $category_update_request->category_name,
			'category_url' => $category_update_request->category_url,
			'category_slug' => $category_update_request->category_slug,
			'front_page' => $category_update_request->show_front_page,
			'front_page_feature' => $category_update_request->front_page_featurette,
			'category_description' => $category_update_request->category_description,
			'category_meta_description' => $category_update_request->category_meta_description,
		]);

		return redirect('/#category_updated');	
	}
	
	public function getCategoriesList(){
		return $this->get();
	}
	
	public function getExistingConnections($current_source_id){
		$category_data = $this->all();
		
		$existing_links = \DB::table('category_links')->where('source_id', '=', $current_source_id)->get();
		
		$sorted_category_info = collect();
		foreach ($category_data as $category_info){
			if($existing_links->contains('category_id',$category_info->category_id)){
				$sorted_category_info->push($category_info);
			}
		}
		
		return $sorted_category_info;
	}
}
