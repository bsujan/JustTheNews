<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AdminActions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
	
		if(!Auth::check() || $request->user()->role != 'admin'){
			return redirect('/');
		}else{
			return $next($request);
		}
    }
}
