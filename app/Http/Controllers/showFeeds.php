<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Feeds;

use App\subscription;
use App\category;
use App\Http\Controllers\subscriptions;

use App;

use Carbon\Carbon;

date_default_timezone_set('America/New_York'); 

class showFeeds extends Controller
{

	//Get and show all the feeds when all feeds route is requested.
    public function allFeeds() {
	
	$rss_feed_sources = DB::table('sources')->where('exclude_from_feed','=',0)->pluck('rss_feed_url');
	
	/*
		The database results are returned as a collection.
		This is normally more powerful than simply having an array.
		In this case however we just need to spit out an array of feed URL's with no extra functionality.
		So the collection will be passed to an array using the toArray() method.
	*/
	$rss_feed_sources = $rss_feed_sources->toArray();
	
		$crawl_per_feed = env("ALL_POST", 33) / 3;
	
		$feed = Feeds::make($rss_feed_sources, $crawl_per_feed);
			
		$data = array(
			'title'      => $feed->get_title(),
			'permalink'  => $feed->get_permalink(),
			'items'      => $feed->get_items(0,env("ALL_POST", 33)),
		);

		return \View::make('feed', $data);
	}
	
	//Show only the feed a user has chosen when they request their feed.
	public function usersChoosenFeeds(){
		$raw_feed_subscriber_list = new subscriptions;
	
		$user_subscriptions = $raw_feed_subscriber_list->userSubscriptionsListPure()->where('current_sub','=','true');
		
		$user_feed_rss_urls = $user_subscriptions->pluck('rss_feed_url');
		$user_feed_rss_urls = $user_feed_rss_urls->toArray();
		
		$crawl_per_feed = env("USER_POST", 50) / 3;
		
		$user_subscribed_feeds = Feeds::make($user_feed_rss_urls, $crawl_per_feed);
			
		$user_feed_subscription_data = array(
			'title'     => $user_subscribed_feeds->get_title(),
			'permalink' => $user_subscribed_feeds->get_permalink(),
			'items'     => $user_subscribed_feeds->get_items(0,env("USER_POST", 50)),
		);
		
		return \View::make('personal_feed',['user_stories' => $user_feed_subscription_data,'user_subscriptions' => $user_subscriptions]);
	}
	
	
	//Create a feed for a given category at it's URL.
	public function categoryFeed($category_url){
		$category_data = category::where('category_url','=',$category_url)->firstOrFail();
		$category_links = DB::table('category_links')->where("category_id","=",$category_data->category_id)->pluck('source_id');
		$full_sources = DB::table('sources')->whereIn('source_id',$category_links)->where('exclude_from_feed','=',0)->get();
		
		$sources_rss_url = $full_sources->pluck('rss_feed_url')->toArray();
	
		$crawl_per_feed = env("ALL_POST", 25) / 3;
	
		$feed = Feeds::make($sources_rss_url, $crawl_per_feed);
			
		$feed_data = array(
			'title'      => $feed->get_title(),
			'permalink'  => $feed->get_permalink(),
			'items'      => $feed->get_items(0,env("ALL_POST", 33)),
		);
		
		return \View::make('cat_feed',['news_items' => $feed_data,'cat' => $category_data,'sources' => $full_sources]);
	}
	
	//Get limited sets of results from categories set to show on the homepage then return them to the homepage.
	public function homePageFeeds(){
		$frontpage_categories = DB::table('categories')->where("front_page","=",1)->get();
		$cat_ids = $frontpage_categories->pluck("category_id");
		
		//Get and map in the source/feed URLs for each category on the front page.		
		$frontpage_categories->map(function($frontpage_category){
			
			$cat_links = DB::table('category_links')->where('category_id','=',$frontpage_category->category_id)->pluck("source_id");
			$source_urls = DB::table('sources')->whereIn('source_id',$cat_links)->where('exclude_from_feed','=',0)->pluck("rss_feed_url");
			
			$frontpage_category->source_ids = $source_urls;
			return $frontpage_category;
		});
		
		
		//For each category on the front page get its source URLs. Pass them to an array, generate the simplePie stories array. Map that array onto the frontpage category data.
		$frontpage_categories->map(function($frontpage_category){
			
			$source_urls = $frontpage_category->source_ids;
			$source_urls = $source_urls->toArray();
			
			$front_page_feed = Feeds::make($source_urls, 3);
			
			$front_page_feed_data = array(
			'title'     => $front_page_feed->get_title(),
			'permalink' => $front_page_feed->get_permalink(),
			'items'     => $front_page_feed->get_items(0,10),
			);
			
			$frontpage_category->front_page_stories = $front_page_feed_data;
			
		});
		

		$frontpage_featured_categories = DB::table('categories')->where("front_page_feature","=",1)->get();
		
		return \View::make('homepage', ['frontpage_data' => $frontpage_categories,'frontpage_features' => $frontpage_featured_categories]);		
	}
}
