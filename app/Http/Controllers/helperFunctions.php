<?php
namespace App\Http\Controllers;

use Carbon\Carbon;

class helperFunctions{
	
	public static function sourceURLStripper($sourcePermalink){
		$parsed = parse_url($sourcePermalink);
		
		$hostname_url = $parsed['host'];
		
		return $hostname_url;
	}
	
	public static function hourAhead($story_time){
		$story_time = Carbon::parse($story_time);
		$current_time = Carbon::now();
		$future_hour = $current_time->addHours(8);
		
		if($story_time->gt($future_hour)){
			return true;
		}else{
			return false;
		}
	}
}