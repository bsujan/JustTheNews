<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\category;

class categoryManager extends Controller
{

	//Response data will echo within the page as json and is then sent to javascript xmlhttprequest to reflect changes within the page.
	private $response_data = [];
	
	//Creates a new category.
	public function createNewCategory(Request $create_category_request){
	
		$addCategory = new category();
	
		$validated_category_data = $addCategory->addNewCategory($create_category_request);
		
		return $validated_category_data;
	}
	
	public function updateCategoryData(Request $create_category_update_request){
		
		$updateCategory = new category();
		
		$validated_updated_category = $updateCategory->processCategoryUpdate($create_category_update_request);
		
		return $validated_updated_category;
	}
	
	//Get category data and pass it to the editor page.
	public function editCategoryInfo($category_id){
				
		$single_category_info = category::where('category_id','=',$category_id)->first();
		
		return view('editCategory',['category_core_information' => $single_category_info]);
	}
	
	//Links a source to a category.
	public function linkSource($source_id,$category_id){

		if($this::linkExist($source_id,$category_id) == false){
			$response_data = ["success" => false,"reason" => "This category link already exist. Please try a different one."];
		}else{
			\DB::table('category_links')->insert(['category_id' => $category_id, 'source_id' => $source_id]);
			$response_data = ["success" => true,"reason" => "The category link was successfully created."];
		}
		
		//Encode the response data to json and echo it. Jquery can parse the json and reflect success and failure in the page this way.
		$response_data = json_encode($response_data);
		echo $response_data;
	}
	
	//Unlinks a source from a category.
	public function removeCategoryLink($source_id,$category_id){
		if($this::linkExist($source_id,$category_id) == false){
			$category_links_list = \DB::table('category_links')->where('source_id', '=', $source_id)->get();
			
			$unique_link_id = $category_links_list->where('category_id','=',$category_id)->pluck('category_link_id')->toArray()[0];
			
			\DB::table('category_links')->where('category_link_id','=',$unique_link_id)->delete();
			
			$response_data = ["success" => true,"reason" => "Category link was successfully removed."];
		}else{
			$response_data = ["success" => false,"reason" => "The category link could not be removed. Either it no longer exist or another error occured."];
		}
		
		//Encode the response data to json and echo it. Jquery can parse the json and reflect success and failure in the page this way.
		$response_data = json_encode($response_data);
		echo $response_data;
	}
	
	//Checks to see if a category source connection already exist or not.
	private function linkExist($source_id,$category_id){
		if(\DB::table('sources')->where('source_id', '=', $source_id)->exists() && category::where('category_id', '=', $category_id)->exists()){
			$existing_source_associations = \DB::table('category_links')->where('source_id', '=', $source_id)->get();
			
			if($existing_source_associations->contains('category_id',$category_id)){
				//The category link exist.
				return false;
			}else{
				//The category link does not exist.
				return true;
			}
		}
	}
	
}
