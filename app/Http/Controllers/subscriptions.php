<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\source;
use App\subscription;

class subscriptions extends Controller
{	
	/*
	* Checks subscriptions of logged in user against all existing feeds.
	* returns view with collection.
	*/
	public function userSubscriptionsList(){
	
		$news_services = \App\source::orderBy('source_name','ASC')->get();
		
		$signed_in_user_id = \Auth::id();
		$user_subscribed_to = \App\subscription::where('user_id','=',$signed_in_user_id)->get();
		
		//Maps in a current_sub value. It is assumed if the value isn't found later as part of then they aren't a subscriber.
		$news_services->map(function($news_service){
			$news_service['current_sub'] = false;
			return $news_service;
		});
		
		foreach($news_services as $news_service){
			if($user_subscribed_to->contains('news_source_id',$news_service->source_id)){
				$news_service->current_sub = true;
			}
		}
		
		return view('subscriptions',['subscribed_list' => $news_services]);
	}
	
	/*
	* Checks subscriptions of logged in user against all existing feeds. Returns only the collection of these subscriptions and not an array.
	* returns collection;
	*/
	public function userSubscriptionsListPure(){
	
		$news_services = \App\source::orderBy('source_name','ASC')->get();
		
		$signed_in_user_id = \Auth::id();
		$user_subscribed_to = \App\subscription::where('user_id','=',$signed_in_user_id)->get();
		
		//Maps in a current_sub value. It is assumed if the value isn't found later as part of then they aren't a subscriber.
		$news_services->map(function($news_service){
			$news_service['current_sub'] = false;
			return $news_service;
		});
		
		foreach($news_services as $news_service){
			if($user_subscribed_to->contains('news_source_id',$news_service->source_id)){
				$news_service->current_sub = true;
			}
		}
		
		return $news_services;
	}
}
