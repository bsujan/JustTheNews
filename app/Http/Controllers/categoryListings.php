<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\category;

class categoryListings extends Controller
{
    public function CompleteCategoryList(){
		$category_list = category::orderBy('category_name', 'ASC')->get();
		
		return view('cat_list',['all_categories' => $category_list]);
	}
}
